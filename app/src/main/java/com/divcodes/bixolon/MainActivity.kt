package com.divcodes.bixolon

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import app.akexorcist.bluetotohspp.library.BluetoothSPP
import app.akexorcist.bluetotohspp.library.BluetoothState
import app.akexorcist.bluetotohspp.library.DeviceList
import com.bixolon.commonlib.BXLCommonConst
import com.bixolon.commonlib.connectivity.searcher.BXLBluetooth
import com.bixolon.labelprinter.BixolonLabelPrinter


class MainActivity : AppCompatActivity() {
    private val APP_VERSION = "1.2"
    lateinit var bt: BluetoothSPP
    lateinit var logTV: TextView
    lateinit var versionTV: TextView
    lateinit var nameTV: TextView
    lateinit var logLL: LinearLayout
    lateinit var printBtn: Button
    lateinit var logsBtn: Button
    lateinit var connectBtn: Button
    lateinit var printer: BixolonLabelPrinter;
    var mIsConnected = false
    var mConnectedDeviceName = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        logTV = findViewById(R.id.logTV)
        versionTV = findViewById(R.id.versionTV)
        nameTV = findViewById(R.id.nameTV)
        logLL = findViewById(R.id.logLL)
        logsBtn = findViewById(R.id.logBtn)
        printBtn = findViewById(R.id.printBtn)
        connectBtn = findViewById(R.id.connectBtn)

        versionTV.text = APP_VERSION

        bt = BluetoothSPP(applicationContext)

        logsBtn.setOnClickListener {
            logLL.visibility = View.VISIBLE
        }

        logTV.setOnClickListener {
            logLL.visibility = View.GONE
        }

        connectBtn.setOnClickListener {
            connect()
        }

        printBtn.setOnClickListener {
            print()
        }

        printer = BixolonLabelPrinter(this, mHandler, null)

        log("Started...")
    }

    private val mHandler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        @SuppressLint("HandlerLeak")
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                BixolonLabelPrinter.MESSAGE_STATE_CHANGE -> when (msg.arg1) {
                    BixolonLabelPrinter.STATE_CONNECTED -> {
                        mIsConnected = true
                        log("connected")
                    }
                    BixolonLabelPrinter.STATE_CONNECTING -> log("connecting", true)
                    BixolonLabelPrinter.STATE_NONE -> {
                        log("not connected", true)
                        mIsConnected = false
                    }
                }
            }
        }
    }

    /*
        private val mHandler: Handler = @SuppressLint("HandlerLeak")
        object : Handler() {
            @SuppressLint("HandlerLeak")
            override fun handleMessage(msg: Message) {
                when (msg.what) {
                    BixolonLabelPrinter.MESSAGE_STATE_CHANGE -> when (msg.arg1) {
                        BixolonLabelPrinter.STATE_CONNECTED -> {
                            mIsConnected = true
                        }
                        BixolonLabelPrinter.STATE_CONNECTING -> log("connecting", true)
                        BixolonLabelPrinter.STATE_NONE -> {
                            log(msg.toString(), true)
                            mIsConnected = false
                        }
                    }
                    BixolonLabelPrinter.MESSAGE_READ -> dispatchMessage(msg)
                    BixolonLabelPrinter.MESSAGE_DEVICE_NAME -> {
                        mConnectedDeviceName = msg.data.getString(BixolonLabelPrinter.DEVICE_NAME)!!
                        log("printer: $mConnectedDeviceName", true)
                    }
                    BixolonLabelPrinter.MESSAGE_TOAST ->
                        log("new msg: ${msg.data.getString(BixolonLabelPrinter.TOAST)}", true)
                    BixolonLabelPrinter.MESSAGE_LOG ->
                        log("new log: ${msg.data.getString(BixolonLabelPrinter.LOG)}", true)
                    BixolonLabelPrinter.MESSAGE_BLUETOOTH_DEVICE_SET -> if (msg.obj == null) {
                        log("No paired device", true)
                    } else {
                        log("paired device true", true)
                    }
                    BixolonLabelPrinter.MESSAGE_NETWORK_DEVICE_SET -> {
                        if (msg.obj == null) {
                            log("No connectable device")
                        }
                    }
                }
            }
        }
    */

    fun print() {
        if (printer.isConnected()) {
            log("yes, connected", true)
        } else {
            log("no, not connected", true)
        }
    }

    fun connect() {

        if (!bt.isBluetoothEnabled || !bt.isBluetoothAvailable) {
            log("bt is not available or not enabled", true)
            return
        }
        log("staring service")

        bt.setupService()
        bt.startService(BluetoothState.DEVICE_OTHER);

        log("service started")

        val intent = Intent(applicationContext, DeviceList::class.java)
        startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == RESULT_OK) {
                log("device selected, connecting")
                if (data != null) {
                    if (data.extras != null) {
                        if (data.extras!!.getString("device_address") != null) {
                            setDevice(data.extras!!.getString("device_address"))
                        }
                    }
                }
            }
        }
    }

    fun setDevice(mac: String? = null) {
        if (mac == null) {
            nameTV.text = ""
            return
        }

        nameTV.text = mac
        printer.connect(mac, BXLCommonConst._PORT_BLUETOOTH_LE)
    }

    override fun onStart() {
        super.onStart()
        if (!bt.isBluetoothEnabled) {
            log("bt is NOT enabled")
        } else {
            log("bt is enabled")
        }
    }

    fun log(text: String, toast: Boolean = false) {
        logTV.append("$text\n")

        if (toast) {
            Toast.makeText(applicationContext, text, Toast.LENGTH_LONG).show()
        }
    }
}